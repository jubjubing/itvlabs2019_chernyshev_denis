#include <iostream>
#include <cassert>
#include <cmath>

unsigned int GetPart(double number) 
{	double Full;
	double Part = modf ( abs (number), &Full);
	Part = Part * 1000;
	unsigned int FullPart = static_cast<unsigned int>(Part);
	FullPart = FullPart / 100 + FullPart % 100 / 10 + FullPart % 10;
	return FullPart;
}

int main()
{	assert(GetPart(0.123) == 6);
	assert(GetPart(111.123) == 6);
	assert(GetPart(234.0) == 0);
	assert(GetPart(0) == 0);
	assert(GetPart(-0.123) == 6);
	assert(GetPart(-111.123) == 6);
	assert(GetPart(-234.0) == 0);
	assert(GetPart(-234.99999999999) == 27); //Error with round() function
	double Number;
	std::cin >> Number;
	std::cout << GetPart(Number)<<"\n";
	std::cin.get();
	std::cin.get();
    return 0;
}

