#include "converttoout.h"
#include <iostream>
#include <math.h> 
std::string convertToOut(double price) {
	double fractpart;
	double intpart;
	fractpart = modf(price, &intpart);
	fractpart = round(fractpart * 100);
	return std::to_string(static_cast<unsigned int>(intpart)) + " roubles " 
		 + std::to_string(static_cast<unsigned int>(fractpart)) + " kopek \n";
}
bool areDoubleSame(double dFirstVal, double dSecondVal)
{
	return std::fabs(dFirstVal - dSecondVal) < 1E-2;
}