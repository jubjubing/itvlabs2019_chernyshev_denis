#include "converttoout.h"
#include <iostream>
#include <cassert>

double getFinalPrice(double length, double volume, double price) 
{
	return volume / 100. * length * 2. * price;
}


int main()
{
	assert(areDoubleSame(getFinalPrice(67, 8.5, 23.7), 269.94));
	assert(convertToOut(269.94)=="269 roubles 94 kopek \n");
	assert(convertToOut(0.34) == "0 roubles 34 kopek \n");
	std::cout << "Enter the length \n";
	double length;
	std::cin >> length;
	std::cout << "Enter the gas consumption per 100 km\n";
	double volume;
	std::cin >> volume;
	std::cout << "Enter the gas price per liter\n";
	double price;
	std::cin >> price;
	std::cout << "Transportation price is ";
	convertToOut(getFinalPrice(length, volume, price));
	std::cin.get();
	std::cin.get();
    return 0;
}

